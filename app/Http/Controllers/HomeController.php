<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function detail()
    {
        return view('frontend.detail.property_details');
    }

    public function listing()
    {
        $ads = Ads::paginate(10);
        return view('frontend.listing.listing',[
            'ads' => $ads
        ]);
    }

    public function about()
    {
        return view('frontend.about.about');
    }

    public function contact()
    {
        return view('frontend.contact.contact');
    }
}
