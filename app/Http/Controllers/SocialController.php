<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite as Socialite;
use App\User;

class SocialController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function redirectFB(){
        return Socialite::driver('facebook')->redirect();
    }

    public function callback($provider){
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        Auth::login($user);
        return redirect()->to('/home');
    }

    public function callbackFB(){
        $getInfo = Socialite::driver('facebook')->user();
        $user = $this->createUser($getInfo,'facebook');
        Auth::login($user);
        return redirect()->to('/home');
    }

    public function createUser($userInfo , $provider){
        try {
            $user = User::where('provider_id', '=', $userInfo->id)->first();
            if (is_null($user)) {
                User::create([
                    'name' => $userInfo->name,
                    'email' => $userInfo->email,
                    'provider' => $provider,
                    'provider_id' => $userInfo->id,
                    'is_admin' => User::NORMAL_USER
                ]);
            } else {
                return $user;
            }
        } catch (\Exception $e) {
            //Authentication failed
            return redirect()
                ->back()
                ->with('status','authentication failed, please try again!'.$e->getMessage());
        }
    }
}
