<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Ads;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $ads = Ads::where('user_id','=',\Auth::user()->id)->get();
        return view('frontend.profile.index',[
            'ads' => $ads
        ]);
    }

    public function updateData(Request  $request){
        $user = User::where('id', '=', \Auth::user()->id)->first();
        if($request->type == 1) {
            if ($request->hasFile('photo')) {
                $name = $request->file('photo')->getClientOriginalName();
                $path = 'uploads/photo';
                $fileName = $path . '/' . $name;
                $request->file('photo')->move($path, $name);
            }
            User::where('id', '=', $user->id)->update([
                'phone_number' => isset($request->phone_number) ? $request->phone_number : $user->phone_number,
                'city_area' => isset($request->city_area) ? $request->city_area : $user->city_area,
                'location' => isset($request->location) ? $request->location : $user->location,
                'name' => isset($request->username) ? $request->username : $user->name,
                'url' => isset($request->url) ? $request->url : $user->url,
                'user_type' => isset($request->user_type) ? $request->user_type : $user->user_type,
                'zip_code' => isset($request->zip_code) ? $request->zip_code : $user->zip_code,
                'avatar' => isset($fileName) ? $fileName : $user->avatar
            ]);

            return back()->with('success','profile update successfully');

        }

        if($request->type == 2) {
            User::where('id', '=', $user->id)->update([
                'email' => $request->email
            ]);

            return back()->with('success','email update successfully');
        }

        if($request->type == 3) {
            if($request->password != '' && $request->c_password != '') {
                if($request->password == $request->c_password) {
                    User::where('id', '=', \Auth::user()->id)->update([
                        'password' => bcrypt($request->password)
                    ]);
                } else {
                    return back()->with('error','password or confirm password not match');
                }
            } else {
                return back()->with('error','password or confirm password not match');
            }
            return back()->with('success','password update successfully');
        }
    }
}
