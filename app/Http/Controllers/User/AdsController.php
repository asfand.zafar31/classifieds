<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\AdImages;
use App\Models\Ads;
use App\User;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function postAdd(){
        return view('frontend.user.ads.create');
    }

    public function storeAdd(Request  $request){
        $user = User::where('id','=',\Auth::user()->id)->first();
        if($request->username == '') {
            $user->update([
                'name' => $request->username,
            ]);
        } else if($request->email == ''){
            $user->update([
                'email' => $request->email,
            ]);
        } else if($request->phone_number == ''){
            $user->update([
                'phone_number' => $request->phone_number,
            ]);
        }
        $ads = Ads::create([
            'title' => $request->title,
            'description' => $request->desc,
            'fee' => $request->price,
            'category' => $request->category_id,
            'location' => $request->location,
            'user_id' => \Auth::user()->id,
            'city' => $request->city,
            'area' => $request->area,
            'type_e' => $request->type_e,
            'condition' => $request->condition,
            'zip_code' => $request->zip,
        ]);
        $this->uploadImages($request,$ads->id);
        return collect([
            'status' => true,
            'message' => 'your ads created successfully'
        ]);
    }

    public function uploadImages($request,$id){
        $count = $request->counter;
        $image = '';
        for ($i = 0; $i < $count; $i++) {
            if ($request->hasFile('image_' . $i)) {
                $file_Name = $request->file('image_' . $i)->getClientOriginalName();
                $path = 'uploads/ads/images';
                $fileName = $path.'/'.$file_Name;
                $image = $fileName;
                $request->file('image_' . $i)->move($path, $file_Name);
                AdImages::create([
                    'ads_id' => $id,
                    'name' => $file_Name,
                    'image' => $fileName
                ]);
            }
        }
        Ads::where('id','=',$id)->update([
           'image' => $image
        ]);
    }
}
