<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{

    const TYPE_ALL = 1;
    const TYPE_SELL = 2;
    const TYPE_BUY = 3;
    const TYPE_RENT = 4;
    const TYPE_EXCHANGE = 5;

    const CONDITION_ALL = 1;
    const CONDITION_NEW = 2;
    const CONDITION_USED = 3;

    protected $fillable = [
      'title','description','fee','category','location','user_id','image','zip_code','city','area','type_e','condition'
    ];
}
