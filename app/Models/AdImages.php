<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdImages extends Model
{
    protected $fillable = [
      'name','image','ads_id'
    ];
}
