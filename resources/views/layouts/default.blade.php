<!DOCTYPE html>
<html lang="en">
<head>
    <title>ClassyAds &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('frontend.partials.headScript')

    @stack('css')
</head>
<body>
@php
    $url = \Request::fullUrl();
    $about = str_contains($url,'about');
    $contact = str_contains($url,'contact');
    $createAds = str_contains($url,'create-ads');
    $profile = str_contains($url,'profile');
@endphp
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    @include('frontend.partials.header')

    @yield('content')


    @if(!$about && !$contact && !$createAds && !$profile)
        @include('include.trending_today')
        @include('include.testimonials')
        @include('include.blog')
    @endif

    @include('frontend.partials.footer',['createAds'=>$createAds,'profile' => $profile])
</div>

@include('frontend.partials.footScripts')
@stack('js')
</body>
</html>
