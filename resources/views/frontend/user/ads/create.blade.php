@extends('layouts.default')


@push('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style>
        .nav-tabs > li {
            padding: 15px;
            border: 1px solid #efefef;
            text-align: center;
            border-bottom: 0px;
            cursor: pointer;
        }
        .nav-tabs > li > a{
            text-align: center;
            cursor: pointer;
            border-bottom: 0px;
        }
        .nav-tabs > li > a{
            color: #000;
        }
        .nav-tabs > li > a:hover{
            color: #30E3ca;
        }
        .nav-tabs > li:hover{
            color: #30E3ca;
        }
        .pdding-top{
            padding-top: 100px;
        }
        .bgColor{
            background: #fff;
            text-align: left;
            padding: 30px 20px;
            border: 1px solid #efefef;
        }
        .customlabel{
        }
    </style>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-container{
            z-index: 999999999999999999;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            cursor: pointer;
            background-color: #fff;
            font-size: 15px;
            font-weight: 300;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
@endpush


@section('content')

    <div class="site-blocks-cover-createPost" style="" data-aos="fade">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center pdding-top">
                <div class="col-md-12 pdding-top">
                    <h1 class="text-left">Publish a new listing</h1>
                    <form id="submitAds" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="bgColor">
                                <label class="customlabel">Select Category*</label>
                                <select class="form-control" required name="category_id" id="category_id">
                                    <option value="1">Real Estate</option>
                                    <option value="2">Real Estate 2</option>
                                    <option value="3">Real Estate 3</option>
                                    <option value="4">Real Estate 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="bgColor">
                                <label class="customlabel">Where is your item located?*</label>
                                <input type="text" id="pac-input" required name="address" class="custom-form-control form-control"
                                       value=""/>
                                <div id="map" style="height: 300px;display: none !important;"></div>
                                <br>
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="customlabel">city*</label>
                                        <input type="text" id="city" name="city" required class="form-control" />
                                    </div>
                                    <div class="col-md-5">
                                        <label class="customlabel">area*</label>
                                        <input type="text" id="area" name="area" required class="form-control" />
                                    </div>
                                    <div class="col-md-2">
                                        <label class="customlabel">zip*</label>
                                        <input type="number" id="zip" name="zip" required class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bgColor">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="customlabel">Your Name*</label>
                                        <input type="text" name="name" id="username" required  class="form-control" value="{{\Auth::user()->name}}" placeholder="enter username" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="customlabel">Mobile Phone</label>
                                        <input type="number" name="phone_number" id="phone_number" required  class="form-control" value="{{\Auth::user()->phone_number}}" placeholder="enter phone number" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="customlabel">E-mail*</label>
                                        <input type="email" name="email" id="email" required  class="form-control" value="{{\Auth::user()->email}}" placeholder="enter email address" />
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bgColor">
                                <label class="customlabel">Price*</label>
                                <input type="number" name="price" id="price" required  class="form-control" placeholder="enter price" />

                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="customlabel">Transaction*</label>
                                        <select class="form-control" required name="type" id="type_e">
                                            <option value="1" selected>All</option>
                                            <option value="2">Sell</option>
                                            <option value="3">Buy</option>
                                            <option value="4">Rent</option>
                                            <option value="5">ExChange</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="Condition">Condition*</label>
                                        <select class="form-control" required name="condition" id="condition">
                                            <option value="1" selected>All</option>
                                            <option value="2">New</option>
                                            <option value="3">Used</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bgColor">
                                <label class="customlabel">Title*</label>
                                <input type="text" name="title" id="title" required class="form-control" placeholder="title" />

                                <br>

                                <label class="customlabel">Description*</label>
                                <textarea class="form-control" id="description" required name="description" cols="12" rows="12">

                                </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bgColor">
                                <label class="customlabel">Photo*</label>
                                <small>You can upload up to 3 pictures per listing</small>
                                <input type="file" name="images[]" multiple id="ssi-upload7"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="button" onclick="submitAds()" class="btn btn-success">Publish item</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>

@endsection



@push('js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&libraries=places&callback=initMap"
        defer>
    </script>
    <script>
        let orgLat = '';
        let orgLong = '';
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: orgLat,
                    lng: orgLong
                },
                zoom: 13
            });
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            var types = document.getElementById('type-selector');
            var strictBounds = document.getElementById('strict-bounds-selector');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            marker.setVisible(false);
            marker.setPosition({
                lat: orgLat,
                lng: orgLong
            });
            marker.setVisible(true);


            autocomplete.addListener('place_changed', function () {
                console.log('place changed');
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                console.log(place);
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                console.log(lat);
                console.log(lng);
                setSelectedLatLng(lat, lng)
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });

            console.log('asdasd');
            google.maps.event.addListener(marker, 'dragend', function (marker) {
                console.log(marker);
                var latLng = marker.latLng;
                currentLatitude = latLng.lat();
                currentLongitude = latLng.lng();
                console.log(currentLatitude);
                console.log(currentLongitude);
                setSelectedLatLng(currentLatitude, currentLongitude)
            });
        }

        function setSelectedLatLng(lat, lng) {
            $('#lats').val(lat);
            $('#lngs').val(lng);
        }
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href="{{asset('ssi-uploader/styles/ssi-uploader.css')}}" rel="stylesheet">
    <script src="{{asset('ssi-uploader/js/ssi-uploader.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        var files_data = [];
        $('#ssi-upload7').ssi_uploader({
            dropZone:true,
            maxNumberOfFiles:3,
            allowed: ['jpg', 'jpeg', 'png'],
            onUpload: function () {
            },
            onEachUpload: function (fileInfo) {
            },
            beforeUpload: function () {
            },
            beforeEachUpload: function (fileInfo, xhr) {
            },
            filesUpload: function (fileInfo) {
                files_data.push(fileInfo);
                $('#files').append(fileInfo);
            }
        });
        function submitAds(){
            console.log('files_data');
            console.log(files_data[0]);
            let category_id = $('#category_id').val();
            let pac_input = $('#pac-input').val();
            let description = $('#description').val();
            let title = $('#title').val();
            let price = $('#price').val();
            let city = $('#city').val();
            let area = $('#area').val();
            let zip = $('#zip').val();
            let condition = $('#condition').val();
            let type_e = $('#type_e').val();
            let username = $('#username').val();
            let email = $('#email').val();
            let phone_number = $('#phone_number').val();
            let files = files_data[0];
            console.log(category_id);
            console.log(pac_input);
            console.log(description);
            console.log(title);
            console.log(city);
            console.log(area);
            console.log(zip);

            if(category_id == '') {
                toastr.error('please select category');
            } else if(pac_input == '') {
                toastr.error('please enter location');
            } else if(city == '') {
                toastr.error('please enter city');
            } else if(area == '') {
                toastr.error('please enter area');
            } else if(zip == '') {
                toastr.error('please enter zip code');
            } else if(price == '') {
                toastr.error('please enter price');
            }  else if(title == '') {
                toastr.error('please enter title');
            }  else if(description == '') {
                toastr.error('please enter description');
            } else {

                let form = new FormData();

                let counter = 0;
                $.each(files, function (key, value) {
                    form.append('image_' + key, value);
                    counter++;
                });

                form.append('category_id', category_id);
                form.append('location', pac_input);
                form.append('desc', description);
                form.append('title', title);
                form.append('city', city);
                form.append('area', area);
                form.append('zip', zip);
                form.append('condition', condition);
                form.append('type_e', type_e);
                form.append('price', price);
                form.append('username', username);
                form.append('email', email);
                form.append('phone_number', phone_number);
                form.append('counter', counter);


                let url = '{{route('user.post.ads')}}';

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    method: 'POST',
                    data: form,

                    cache: false,
                    contentType: false,
                    processData: false,
                    url: url,

                    success: function (response) {
                        console.log('response');
                        console.log(response);
                        if (response.status == true) {
                            toastr.success(response.message);
                            setTimeout(() => {
                                window.location.reload();
                            }, 1500);
                        }
                    },

                    error: function (error) {
                        console.log('error');
                        console.log(error);
                    }
                });
            }
        }
    </script>
@endpush
