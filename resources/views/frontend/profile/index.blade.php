@extends('layouts.default')


@push('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .nav-tabs > li {
            padding: 15px;
            border: 1px solid #efefef;
            text-align: center;
            border-bottom: 0px;
            cursor: pointer;
        }
        .nav-tabs > li > a{
            text-align: center;
            cursor: pointer;
            border-bottom: 0px;
        }
        .nav-tabs > li > a{
            color: #000;
        }
        .nav-tabs > li > a:hover{
            color: #30E3ca;
        }
        .nav-tabs > li:hover{
            color: #30E3ca;
        }
        .pdding-top{
            padding-top: 100px;
            padding-bottom: 50px;
        }
        .site-blocks-cover-createPost{
            background: #efefef;
        }
        .custom-background{
            background: #fff;
        }
        .uploadLabael {
            cursor: pointer;
            color: lightskyblue;
            text-decoration: underline;
        }
        #upload-photo {
            opacity: 0;
            position: absolute;
            z-index: -1;
        }
    </style>
@endpush


@section('content')

    <div class="site-blocks-cover-createPost" style="" data-aos="fade">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center pdding-top">
                @if(session()->has('success'))
                    <div class="row" style="width: 100%;">
                        <div class="col-md-12" style="padding: 0px;">
                            <div class="alert alert-success" style="margin-bottom: 0px;">
                                {{session()->get('success')}}
                            </div>
                        </div>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="row" style="width: 100%;">
                        <div class="col-md-12" style="padding: 0px;">
                            <div class="alert alert-danger" style="margin-bottom: 0px;">
                                {{session()->get('error')}}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-md-12 custom-background pdding-top">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">My Profile</a></li>
                        <li><a data-toggle="tab" href="#menu1">My Listing</a></li>
                        <li><a data-toggle="tab" href="#menu2">Change Email</a></li>
                        <li><a data-toggle="tab" href="#menu3">Change Password</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active show">
                            <br>
                            <h3 class="text-left">My Profile</h3>
                            <hr>
                            <form action="{{route('update.user.data')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="type" value="1" />
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>Name*</label>
                                        <input type="text" name="username" class="form-control" value="{{\Auth::user()->name}}" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label class="uploadLabael" for="upload-photo">update avatar</label>
                                        <input type="file" name="photo" id="upload-photo" onchange="loadFile(event)" /><br>
                                        <img id="output"  src="/{{\Auth::user()->avatar}}" onerror="this.src='{{asset('dummyimage.png')}}'" style="border-radius:4px;width: 130px;height: 130px;" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <b>{{\Auth::user()->email}}</b>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>Phone Number*</label>
                                        <input type="number" name="phone_number" placeholder="enter phone number" class="form-control" value="{{\Auth::user()->phone_number}}" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>Location*</label>
                                        <input type="text" name="location" placeholder="enter your location" class="form-control" value="{{\Auth::user()->location}}" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>City Area</label>
                                        <input type="text" name="city_area" placeholder="enter city area" class="form-control" value="{{\Auth::user()->city_area}}" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>ZIP</label>
                                        <input type="number" name="zip_code" placeholder="enter zip code" class="form-control" value="{{\Auth::user()->zip_code}}" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>User Type</label>
                                        <select class="form-control" name="user_type">
                                            <option value="1" selected>User</option>
                                            <option value="2">Company</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <label>Website</label>
                                        <input type="text" name="url" value="{{\Auth::user()->url}}" placeholder="enter your url" class="form-control" />
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <button type="submit" class="btn btn-outline-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <h3 class="text-left">Ads Listing</h3>
                            <hr>
                            <div class="row">
                                @foreach($ads as $ad)
                                <div class="col-lg-4">
                                    <div class="d-block d-md-flex listing vertical">
                                        <a href="#" class="img d-block" style="background-image: url('/{{$ad->image}}')"></a>
                                        <div class="lh-content">
                                            <span class="category">Cars &amp; Vehicles</span>
                                            <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                                            <h3><a href="#">{{$ad->title}}</a></h3>
                                            <address>{{$ad->location}}</address>
                                            <p class="mb-0">
                                                <span class="icon-star text-warning"></span>
                                                <span class="icon-star text-warning"></span>
                                                <span class="icon-star text-warning"></span>
                                                <span class="icon-star text-warning"></span>
                                                <span class="icon-star text-secondary"></span>
                                                <span class="review">(3 Reviews)</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <br>
                            <h3 class="text-left">Change Email</h3>
                            <hr>
                            <form method="POST" action="{{route('update.user.data')}}">
                                @csrf
                                <input type="hidden" name="type" value="2" />
                                <div class="col-md-6 text-left">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="enter email address" />
                                </div>
                                <br>
                                <div class="col-md-6 text-left">
                                    <button type="submit" class="btn btn-outline-primary">Update</button>
                                </div>
                            </form>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <br>
                            <h3 class="text-left">Change Password</h3>
                            <hr>
                            <form method="POST" action="{{route('update.user.data')}}">
                                @csrf
                                <input type="hidden" name="type" value="3" />
                                <div class="col-md-6 text-left">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="enter password" />
                                </div>
                                <br>
                                <div class="col-md-6 text-left">
                                    <label>Confirm Password</label>
                                    <input type="password" name="c_password" class="form-control" placeholder="enter confirm password" />
                                </div>
                                <br>
                                <div class="col-md-6 text-left">
                                    <button type="submit" class="btn btn-outline-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('js')
    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
