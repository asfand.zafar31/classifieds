<header class="site-navbar container py-0 bg-white" role="banner">

    <!-- <div class="container"> -->
    <div class="row align-items-center">

        <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.html" class="text-black mb-0">Classy<span class="text-primary">Ads</span>  </a></h1>
        </div>
        <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

                <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                    <li class="{{ in_array(Route::currentRouteName(), ['home']) ? ' active' : ''  }}"><a href="{{route('home')}}">Home</a></li>
                    <li class="{{ in_array(Route::currentRouteName(), ['listing']) ? ' active' : ''  }}"><a href="{{route('listing')}}">Ads</a></li>
                    <li class="{{ in_array(Route::currentRouteName(), ['about']) ? ' active' : ''  }}"><a href="{{route('about')}}">About</a></li>
                    <li class="{{ in_array(Route::currentRouteName(), ['contact']) ? ' active' : ''  }}"><a href="{{route('contact')}}">Contact</a></li>
                    @if(!\Auth::user())
                        <li class="ml-xl-3 login"><a href="{{url('/login')}}"><span class="border-left pl-xl-4"></span>Log In</a></li>
                        <li><a href="{{url('/register')}}">Register</a></li>
                    @else
                        <li class="has-children">
                            <a href="javascript:;">My Account</a>
                            <ul class="dropdown">
                                <li style="border-bottom: 1px solid #efefef"><a href="javascript:;">{{\Auth::user()->name}}</a></li>
                                <li><a href="{{route('user.profile.index')}}">My Profile</a></li>
                                <li><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                        </li>
                    @endif

                    <li><a href="{{route('user.create.ads')}}" class="cta"><span class="bg-primary text-white rounded">+ Post an Ad</span></a></li>
                </ul>
            </nav>
        </div>


        <div class="d-inline-block d-xl-none ml-auto py-3 col-6 text-right" style="position: relative; top: 3px;">
            <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
        </div>

    </div>
    <!-- </div> -->

</header>
