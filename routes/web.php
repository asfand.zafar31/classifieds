<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/home');
});

Auth::routes();

Route::get('/auth/redirect/{provider}','SocialController@redirect');
Route::get('/googlelogin/callback/{provider}','SocialController@callback');
Route::get('/logout','Auth\LoginController@logout');

Route::get('/redirect', 'SocialController@redirectFB');
Route::get('/callback','SocialController@callbackFB');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detail', 'HomeController@detail')->name('detail');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/listing', 'HomeController@listing')->name('listing');
Route::get('/about', 'HomeController@about')->name('about');

Route::prefix('user')->middleware(['auth'])->namespace('User')->group(function(){
    Route::get('create-ads','AdsController@postAdd')->name('user.create.ads');
    Route::post('post-ads','AdsController@storeAdd')->name('user.post.ads');
    Route::get('profile','ProfileController@index')->name('user.profile.index');
    Route::post('update-profile','ProfileController@updateData')->name('update.user.data');
});
